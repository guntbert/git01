﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace git01
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        ///<summary> das ist ein XML-Kommentar </summary>
        ///<param name="sender"> das Objekt, auf dem der event ausgelöst wurde</param>
        ///<param name="e"> Details des events </param>
        private void btnTest_Click(object sender, EventArgs e)
        {
            this.Text = "28.4.2015";
            btnTest.BackColor=Color.Red;
            btntest.Text="geklickt!";
        }
    }
}